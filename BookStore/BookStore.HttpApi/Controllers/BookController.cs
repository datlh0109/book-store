﻿using BookStore.Application.Contracts.Commands.BookCommand;
using BookStore.Application.Contracts.Queries.BookQuery;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.HttpApi.Controllers
{
    /// <summary>
    /// Quản lý sách
    /// </summary>
    [Route("api/books")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookQuery _bookQuery;
        private readonly IMediator _mediator;

        /// <summary>
        /// BookController
        /// </summary>
        public BookController(IBookQuery bookQuery, IMediator mediator)
        {
            _bookQuery = bookQuery;
            _mediator = mediator;
        }

        /// <summary>
        /// Api get danh sách Book
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetBooks([FromQuery] string code)
        {
            var data = await _bookQuery.GetBooks(code);
            return Ok(data);
        }

        /// <summary>
        /// Api get danh sách Book
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateBook([FromBody] CreateBookCommand command)
        {
            await _mediator.Send(command);
            return Ok();
        }
    }
}
