﻿using BookStore.Application.Contracts.Queries.BookQuery;
using BookStore.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Application.Queries.BookQuery
{
    public class BookQuery : IBookQuery
    {
        private readonly IUnitOfWork _uow;
        public BookQuery(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public async Task<List<BookDto>> GetBooks(string code)
        {
            var books = await _uow.BookRepo.GetBookObjBy(code);
            return books.Select(x => new BookDto() { Code = x.Code, Name = x.Name }).ToList();
        }
    }
}
