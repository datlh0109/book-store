﻿using BookStore.Application.Contracts.Queries.BookQuery;
using BookStore.Application.Queries.BookQuery;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace BookStore.Application
{
    public static class ApplicationServiceConfig
    {
        public static IServiceCollection AddBookStoreApplicationService(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.Load("BookStore.Application"));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(TransactionBehaviour<,>));
            services.AddScoped<IBookQuery, BookQuery>();
            return services;
        }
    }
}
