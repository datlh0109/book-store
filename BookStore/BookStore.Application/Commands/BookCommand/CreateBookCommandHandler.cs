﻿using BookStore.Application.Contracts.Commands.BookCommand;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Application.Commands.BookCommand
{
    public class CreateBookCommandHandler : IRequestHandler<CreateBookCommand, bool>
    {
        public async Task<bool> Handle(CreateBookCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            return true;
        }
    }
}
