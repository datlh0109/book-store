﻿using BookStore.Domain;
using BookStore.Domain.BookAggregate;
using BookStore.EntityFrameworkCore.EntityFrameworkCore;

namespace BookStore.EntityFrameworkCore.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BookStoreDbContext _context;
        private IBookRepository _bookRepo;
        public UnitOfWork(BookStoreDbContext context)
        {
            _context = context;
        }

        public IBookRepository BookRepo
        {
            get { return _bookRepo ??= new BookRepository(_context); }
        }

    }
}
