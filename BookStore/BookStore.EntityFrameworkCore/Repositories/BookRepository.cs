﻿using BookStore.Domain.BookAggregate;
using BookStore.Domain.BookAggregate.ValueObjects;
using BookStore.EntityFrameworkCore.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.EntityFrameworkCore.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly BookStoreDbContext _context;
        public BookRepository(BookStoreDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<List<BookObj>> GetBookObjBy(string code)
        {
            return await _context.Books.Where(x => x.Code.Contains(code))
                .Select(x => new BookObj(x.Id, x.Code, x.Name)).ToListAsync();
        }
    }
}
