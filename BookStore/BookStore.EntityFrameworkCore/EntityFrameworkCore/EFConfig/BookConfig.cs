﻿using BookStore.Domain.BookAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookStore.EntityFrameworkCore.EntityFrameworkCore.EFConfig
{
    public class BookConfig : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.ToTable("Books");
            builder.HasQueryFilter(x => !x.IsDeleted);
            builder.HasIndex(x => new { x.Code, x.IsDeleted }).IsUnique().HasFilter("[IsDeleted] = 0");
            builder.Property(x => x.Code).IsRequired().HasMaxLength(20);
            builder.Property(x => x.Name).HasMaxLength(1000);
            builder.Property(x => x.Author).HasMaxLength(200);
            builder.Property(x => x.Price);
        }
    }
}
