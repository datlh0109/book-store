﻿using System;

namespace BookStore.Domain.BookAggregate.ValueObjects
{
    public class BookObj
    {
        public BookObj()
        {

        }
        public BookObj(Guid id, string code, string name)
        {
            Id = id;
            Code = code;
            Name = name;
        }

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
