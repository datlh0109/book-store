﻿using BookStore.Domain.BookAggregate.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Domain.BookAggregate
{
    public interface IBookRepository
    {
        Task<List<BookObj>> GetBookObjBy(string code);
    }
}
