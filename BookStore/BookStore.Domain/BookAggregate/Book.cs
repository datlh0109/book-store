﻿using System;

namespace BookStore.Domain.BookAggregate
{
    public class Book : IBaseEntity
    {
        public Book()
        {

        }

        public Book(string code, string name, string author, decimal price)
        {
            Code = code;
            Name = name;
            Author = author;
            Price = price;
        }

        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public Guid? CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public Guid? UpdateUser { get; set; }
        public Guid? DeleteUser { get; set; }
        public DateTime? DeleteTime { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public decimal Price { get; set; }
    }
}
