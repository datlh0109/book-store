﻿using BookStore.Domain.BookAggregate;
using System;

namespace BookStore.Domain
{
    public interface IUnitOfWork
    {
        IBookRepository BookRepo { get; }
        
    }

}
