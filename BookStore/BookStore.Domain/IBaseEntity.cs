﻿using System;

namespace BookStore.Domain
{
    public interface IBaseEntity
    {
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public Guid? CreateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public Guid? UpdateUser { get; set; }
        public Guid? DeleteUser { get; set; }
        public DateTime? DeleteTime { get; set; }
    }
}
