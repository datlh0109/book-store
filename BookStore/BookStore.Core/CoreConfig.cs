﻿using Microsoft.Extensions.DependencyInjection;

namespace BookStore.Core
{
    public static class CoreConfig
    {
        public static IServiceCollection AddBookStoreCore(this IServiceCollection services)
        {
            return services;
        }
    }
}
