﻿using System.Linq;

namespace System.Collections.Generic
{
    /// <summary>
    /// Extension methods for <see cref="IList{T}"/>.
    /// </summary>
    public static class AbpListExtensions
    {
        public static T GetOrAdd<T>(this IList<T> source, Func<T, bool> selector, Func<T> factory)
        {
            var item = source.FirstOrDefault(selector);

            if (item == null)
            {
                item = factory();
                source.Add(item);
            }

            return item;
        }
    }
}
