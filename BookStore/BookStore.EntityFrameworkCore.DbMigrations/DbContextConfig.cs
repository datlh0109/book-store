﻿using BookStore.Domain;
using BookStore.EntityFrameworkCore.DbMigrations;
using BookStore.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BookStore.EntityFrameworkCore.EntityFrameworkCore
{
    public static class DbContextConfig
    {
        public static IServiceCollection AddBookStoreEFConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddEntityFrameworkSqlServer()
                     .AddDbContext<BookStoreDbContext>(options =>
                     {
                         options.UseSqlServer(configuration.GetConnectionString("Default"),
                             sqlServerOptionsAction: sqlOptions =>
                             {
                                 //sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                                 sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                             });
                     }  //Showing explicitly that the DbContext is shared across the HTTP request scope (graph of objects started in the HTTP request)
                  );

            services.AddScoped<DbInit>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            return services;
        }
    }
}
