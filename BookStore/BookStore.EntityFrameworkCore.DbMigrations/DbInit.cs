﻿using BookStore.EntityFrameworkCore.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace BookStore.EntityFrameworkCore.DbMigrations
{
    public class DbInit
    {
        private readonly BookStoreDbContext _context;
        private readonly ILogger _logger;

        public DbInit(BookStoreDbContext context, ILogger<DbInit> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void MigrateDatabase()
        {
            _context.Database.Migrate();
        }

        public void Seed()
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _logger.LogInformation("Seed data: Start");

                    // seed data here

                    _context.SaveChanges();
                    transaction.Commit();
                    _logger.LogInformation("Seed data: Finish");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    transaction.Rollback();
                    _logger.LogInformation("Seed data: ERROR");
                }
            }
        }
    }
}
