﻿using BookStore.EntityFrameworkCore.DbMigrations;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.EntityFrameworkCore.EntityFrameworkCore
{
    public static class DbContextMigrate
    {
        public static void UseBookStoreDbContextMigrate(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var initializer = scope.ServiceProvider.GetService<DbInit>();
                // make sure db migration on startup
                initializer.MigrateDatabase();
                // seed data
                initializer.Seed();
            }
        }
    }
}
