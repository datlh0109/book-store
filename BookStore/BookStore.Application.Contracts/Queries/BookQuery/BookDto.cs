﻿namespace BookStore.Application.Contracts.Queries.BookQuery
{
    public class BookDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
