﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Application.Contracts.Queries.BookQuery
{
    public interface IBookQuery
    {
        public Task<List<BookDto>> GetBooks(string code);
    }
}
