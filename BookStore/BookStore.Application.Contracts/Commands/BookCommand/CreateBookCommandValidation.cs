﻿using BookStore.Domain.Shared.Properties;
using FluentValidation;

namespace BookStore.Application.Contracts.Commands.BookCommand
{
    public class CreateBookCommandValidation : AbstractValidator<CreateBookCommand>
    {
        public CreateBookCommandValidation()
        {
            RuleFor(x => x.Code).NotEmpty().WithMessage(string.Format(BookStoreResource.MSG_NOT_EMPTY_ERROR, "Code"))
              .MaximumLength(20).WithMessage(string.Format(BookStoreResource.MSG_MAX_LENGTH_ERROR, "Code", 20));
        }
    }
}
