FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build

ENV APP_HOME /opt/app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

#COPY BookStore/*.csproj $APP_HOME/
COPY *.sln .
COPY Crosscutting/BaseUtility/*.csproj ./Crosscutting/BaseUtility/
COPY BookStore/BookStore.Application/*.csproj ./BookStore/BookStore.Application/
COPY BookStore/BookStore.Application.Contracts/*.csproj ./BookStore/BookStore.Application.Contracts/
COPY BookStore/BookStore.Core/*.csproj ./BookStore/BookStore.Core/
COPY BookStore/BookStore.Domain/*.csproj ./BookStore/BookStore.Domain/
COPY BookStore/BookStore.Domain.Shared/*.csproj ./BookStore/BookStore.Domain.Shared/
COPY BookStore/BookStore.EntityFrameworkCore/*.csproj ./BookStore/BookStore.EntityFrameworkCore/
COPY BookStore/BookStore.EntityFrameworkCore.DbMigrations/*.csproj ./BookStore/BookStore.EntityFrameworkCore.DbMigrations/
COPY BookStore/BookStore.HttpApi/*.csproj ./BookStore/BookStore.HttpApi/
COPY BookStore/BookStore.Service/*.csproj ./BookStore/BookStore.Service/
RUN dotnet restore

COPY . $APP_HOME/

WORKDIR $APP_HOME/BookStore/BookStore.Service
RUN dotnet publish -c Release -o out 

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS final
WORKDIR /app
EXPOSE 80
EXPOSE 443

COPY --from=build /opt/app/BookStore/BookStore.Service/out ./
ENTRYPOINT ["dotnet", "BookStore.Service.dll"]
