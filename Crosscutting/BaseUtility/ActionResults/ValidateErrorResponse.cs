﻿namespace BaseUtility.ActionResults
{
    /// <summary>
    /// Validate Error Response
    /// </summary>
    public class ValidateErrorResponse
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="field"></param>
        /// <param name="message"></param>
        public ValidateErrorResponse(string field, string message)
        {
            Field = field;
            Message = message;
        }

        /// <summary>
        /// Field
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
    }
}
