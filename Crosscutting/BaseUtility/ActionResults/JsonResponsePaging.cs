﻿namespace BaseUtility.ActionResults
{
    /// <summary>
    /// JsonResponsePaging
    /// </summary>
    public class JsonResponsePaging
    {
        /// <summary>
        /// Total Count
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// Page Number
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Page Size
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Total Pages
        /// </summary>
        public int TotalPages { get; set; }

    }
}
