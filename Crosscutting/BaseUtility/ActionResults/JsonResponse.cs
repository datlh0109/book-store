﻿using System.Collections.Generic;

namespace BaseUtility.ActionResults
{
    /// <summary>
    /// Json Response
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonResponse<T>
    {
        /// <summary>
        /// message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Developer Message
        /// </summary>
        public object DeveloperMessage { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Paging
        /// </summary>
        public JsonResponsePaging Paging { get; set; }

        /// <summary>
        /// Errors
        /// </summary>
        public List<ValidateErrorResponse> Errors { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public JsonResponse() { }

        /// <summary>
        ///  Full optional constructor 
        /// </summary>
        public JsonResponse(string message = null, string developerMessage = null, T data = default,
            JsonResponsePaging paging = null, List<ValidateErrorResponse> errors = null)
        {
            Message = message;
            DeveloperMessage = developerMessage;
            Data = data;

            if (paging != null)
            {
                Paging = paging;
            }

            Errors = errors;
        }

        /// <summary>
        /// Ok
        /// </summary>
        /// <returns></returns>
        public static JsonResponse<T> Ok()
        {
            return new JsonResponse<T>();
        }

        /// <summary>
        /// Ok
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <param name="paging"></param>
        /// <returns></returns>
        public static JsonResponse<T> Ok(string message = null, T data = default, JsonResponsePaging paging = null)
        {
            return new JsonResponse<T>(data: data, message: message, paging: paging);
        }

        /// <summary>
        /// BadRequest
        /// </summary>
        /// <returns></returns>
        public static JsonResponse<T> ValidateError(List<ValidateErrorResponse> error = null)
        {
            return new JsonResponse<T>(errors: error);
        }
    }
}
