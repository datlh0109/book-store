﻿using System;
using System.Globalization;

namespace BaseUtility.Exceptions
{
    /// <summary>
    /// Exception type for validate exceptions
    /// </summary>
    public class BusinessException : Exception
    {
        public BusinessException()
        { }

        public BusinessException(string message, params object[] args) : base(string.Format(CultureInfo.CurrentCulture,
           message, args))
        {
        }

        public BusinessException(string message)
            : base(message)
        { }

        public BusinessException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
