﻿using BaseUtility.ActionResults;
using BaseUtility.Properties;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace BaseUtility.Exceptions
{
    /// <summary>
    /// HttpGlobalExceptionFilter
    /// </summary>
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {

        private readonly IHostEnvironment _env;
        private readonly ILogger<HttpGlobalExceptionFilter> _logger;

        /// <summary>
        /// HttpGlobalExceptionFilter Constructor
        /// </summary>
        /// <param name="env"></param>
        /// <param name="logger"></param>
        public HttpGlobalExceptionFilter(IHostEnvironment env, ILogger<HttpGlobalExceptionFilter> logger)
        {
            _env = env;
            _logger = logger;
        }

        /// <summary>
        /// OnException
        /// </summary>
        /// <param name="context"></param>
        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            var developerMessage = "";

            while (exception != null)
            {
                developerMessage += "\r\n--------------------------------------------------\r\n";
                developerMessage += $"{exception.Message}\r\n{exception.StackTrace}";
                exception = exception.InnerException;
            }

            _logger.LogError(new EventId(context.Exception.HResult), context.Exception, developerMessage);

            var json = new JsonResponse<object>
            {
                Message = context.Exception.Message
            };

            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != "Hotfix" && !_env.IsProduction())
            {
                json.DeveloperMessage = developerMessage;
            }

            // 400 Bad Request
            if (context.Exception.GetType() == typeof(BusinessException))
            {
                context.Result = new BadRequestObjectResult(json);
            }
            // 500 Internal Server Error
            else
            {
                json.Message = BaseUtilityResource.MSG_SYSTEM_ERROR;
                context.Result = new InternalServerErrorObjectResult(json);
            }

            context.ExceptionHandled = true;
        }
    }
}
