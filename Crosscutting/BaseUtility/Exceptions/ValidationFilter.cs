﻿using BaseUtility.ActionResults;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace BaseUtility.Exceptions
{
    public class ValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = context.ModelState.Where(x => x.Value.Errors.Count > 0)
                    .Select(x => new ValidateErrorResponse(x.Key, x.Value.Errors.FirstOrDefault()?.ErrorMessage)).ToList();

                context.Result = new ValidateErrorObjectResult(JsonResponse<object>.ValidateError(errors));
                return;
            }
            await next();
        }
    }
}
