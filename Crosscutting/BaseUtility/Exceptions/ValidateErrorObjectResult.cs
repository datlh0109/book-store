﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BaseUtility.Exceptions
{
    /// <summary>
    /// Internal Server Error ObjectResult. Status Code 422
    /// </summary>
    public class ValidateErrorObjectResult : ObjectResult
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="error"></param>
        public ValidateErrorObjectResult(object error)
            : base(error)
        {
            StatusCode = StatusCodes.Status422UnprocessableEntity;
        }
    }
}
